FROM php:7.2.17-apache

MAINTAINER Nikolay Ryabkov <nikolay.ryabkov@sibers.com>

# Install system basics
RUN apt-get update && apt-get install -y \
        zip \
        acl \
    && rm -rf /var/lib/apt/lists/*

# Install some PHP extensions
RUN docker-php-ext-install pdo_mysql

# Install composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('SHA384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php --filename=composer \
    && php -r "unlink('composer-setup.php');" \
    && mv composer /usr/local/bin/composer

# Install and enable remote Xdebug
ENV XDEBUG_INI_PATH /usr/local/etc/php/conf.d/xdebug.ini
RUN pecl install xdebug-2.7.1 && \
    echo "[xdebug]" > ${XDEBUG_INI_PATH} && \
    echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" >> ${XDEBUG_INI_PATH} && \
    echo "xdebug.remote_enable=1" >> ${XDEBUG_INI_PATH} && \
    echo "xdebug.remote_connect_back=1" >> ${XDEBUG_INI_PATH} && \
    echo "xdebug.remote_port=9000" >> ${XDEBUG_INI_PATH}

# Use the default production configuration for PHP
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

# Override with custom PHP settings
COPY ./config/php.ini $PHP_INI_DIR/conf.d

# Enable mod_rewrite and change DocumentRoot
ENV APACHE_DOCUMENT_ROOT /app/web
RUN a2enmod rewrite \
    && sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf \
    && sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf

# Change work directory
WORKDIR /app
